﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;

namespace Zadatak2.Pages {
  public class Loto6od45Model : PageModel {
    private readonly ILogger<Loto6od45Model> _logger;

    public Loto6od45Model(ILogger<Loto6od45Model> logger) {
      _logger = logger;
    }

    public void OnGet() {
      Brojevi = Loto.GenerirajBrojeve(6, 45);
    }

    public SortedSet<int> Brojevi;
  }
}
