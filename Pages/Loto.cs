using System;
using System.Collections.Generic;

namespace Zadatak2.Pages {
  public static class Loto {
    public static SortedSet<int> GenerirajBrojeve(int kolicina, int maksVrijednost) {
      SortedSet<int> brojevi = new SortedSet<int>();

      while (brojevi.Count < kolicina) {
        brojevi.Add(new Random().Next() % maksVrijednost + 1);
      }

      return brojevi;
    }
  }
}
